""" Update a domain record at DigitalOcean """
import os
import sys
import time
import json
import logging
import requests
import miniupnpc

# when running from "/usr/bin/python3 /whatever/this/is/main.py" make sure the correct cwd is used
os.chdir(sys.path[0])

def setup_logging():
    """ Sets up the logging module """

    path   = "logs/activity.log"
    logger = logging.Logger(path)

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    # create a file handler
    handler = logging.FileHandler(path)
    handler.setLevel(logging.INFO)

    # create a logging format
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)

    # add the handlers to the logger
    logger.addHandler(handler)

    return logger

def load_json(filename):
    """ Reads a JSON file and returns it as an array """
    file_handle = open(filename)
    config = json.load(file_handle)
    file_handle.close()
    return config

def list_domains():
    """ Queries the Domain List endpoint
        Is also used as the generic 'Is Network Available' function """
    return send_request(API_PATH, "domains")

def check_domain():
    """ Checks whether the domain exists for this user """
    endpoint = "domains/" + API_SETTINGS['domain']
    return send_request(API_PATH, endpoint)

def check_sub_domain():
    """ Checks whether the subdomain exists for this domain """
    # first get all domain records
    endpoint = "domains/" + API_SETTINGS['domain'] + "/records"
    records  = send_request(API_PATH, endpoint)
    if not records:
        return False
    for record in records["domain_records"]:
        if record["name"] == API_SETTINGS["subdomain"]:
            # we only want A records
            if record["type"] == "A":
                return record
    return False

def create_sub_domain_record(ip):
    """ Creates the sub domain record if it does not exist """
    endpoint = "domains/" + API_SETTINGS['domain'] + "/records"
    data     = {
        "type": "A",
        "name": API_SETTINGS['subdomain'],
        "data": ip,
        "priority": None,
        "port": None,
        "weight": None
    }
    return send_request(API_PATH, endpoint, data)

def update_sub_domain_record(sub_domain_id, ip):
    """ Updates the sub domain record """
    endpoint = "domains/" + API_SETTINGS['domain'] + "/records/" + str(sub_domain_id)
    data     = {
        "type": "A",
        "name": API_SETTINGS['subdomain'],
        "data": ip,
        "priority": None,
        "port": None,
        "weight": None
    }
    return send_request(API_PATH, endpoint, data, True)

def send_request(api_path, endpoint = False, data = False, put = False):
    """ Sends a request to the endpoint with arguments """
    try:

        url = api_path
        if endpoint:
            url = url + endpoint

        headers = {
            'Content-type': 'application/json',
            'Authorization': 'Bearer ' + API_SETTINGS["token"]
        }

        # if this has data
        if data:
            # if this is a PUT
            if put:
                request = requests.put(url, headers = headers, data = json.dumps(data))
            # if this is a POST
            else:
                request = requests.post(url, headers = headers, data = json.dumps(data))
        # else GET it
        else:
            request = requests.get(url, headers = headers)

        if request.status_code == 200 or request.status_code == 201:
            return request.json()
        else:
            LOGGER.error(request.text)
            return False

    except Exception:
        LOGGER.exception("Request")
        return False

def open_ports():
    """ Uses miniupnpc to forward specified ports from the router to the device """
    if API_SETTINGS["ports"]:
        for port in API_SETTINGS["ports"]:

            port_number = API_SETTINGS["ports"][port]
            port_name   = "{} {}".format(port, port_number)

            try:
                upnp = miniupnpc.UPnP()
                # AddPortMapping(externalPort, protocol, internalHost, internalPort, desc, remoteHost)
                upnp.addportmapping(port_number, 'TCP', upnp.lanaddr, port_number, port_name, '')

            except Exception as exception:
                """ I'm having real difficulty getting the python miniupnpc bindings to work
                    so if it throws this exception, let's try just using the command line
                    as we're assuming a linux environment and miniupnpc is installed """
                if str(exception) == "UnknownError":
                    LOGGER.info("Miniupnpc failed. Attempting CL")
                    try:
                        from subprocess import call
                        call(["upnpc", "-e", port_name, "-r", str(port_number), "TCP"])
                    except Exception:
                        LOGGER.exception("Command Line")


LOGGER       = setup_logging()
API_PATH     = "https://api.digitalocean.com/v2/"
API_SETTINGS = load_json("settings.json")

# wait until the network is ready
while True:
    if list_domains():
        break
    else:
        time.sleep(5)

# make sure the domain exists in the DNS records
if check_domain():
    sub_domain     = check_sub_domain()
    public_address = send_request("https://api.ipify.org?format=json")
    if sub_domain:
        # if the IP address doesn't match the public ip, update it
        if sub_domain["data"] != public_address["ip"]:
            update_sub_domain = update_sub_domain_record(sub_domain["id"], public_address["ip"])
            if update_sub_domain:
                LOGGER.info("Record %s for %s updated with IP %s", API_SETTINGS["subdomain"], API_SETTINGS["domain"], public_address["ip"])
            else:
                # We should probably be catching exceptions instead...
                LOGGER.error("Something went wrong when updating record")
    else:
        # if it doesn't, create it with the public ip address
        create_sub_domain = create_sub_domain_record(public_address["ip"])
        if create_sub_domain:
            LOGGER.info("Record %s for %s created with IP %s", API_SETTINGS["subdomain"], API_SETTINGS["domain"], public_address["ip"])
        else:
            # We should probably be catching exceptions instead...
            LOGGER.error("Something went wrong when creating record")
else:
    LOGGER.error("TLD does not exist in DigitalOcean registry for this account")

open_ports()