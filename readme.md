# RPi Semi-Dynamic DNS through DigitalOcean

When run, this updates a specified domain and sub-domain with the current public IP address for the device. This also forwards any ports from the router to the device, as specified in settings.json

1. Query DigitalOcean for existence of Domain
2. Find Record for sub-domain
3. Create record if sub-domain does not exist
4. Change IP Address for sub-domain if it does not match
5. Use upnpc to forward ports from the router

I use this to run a Seafile server through a Raspberry Pi that changes office every couple of months. As it is non-mission-critical then the 24-48 hours DNS propagation window doesn't affect us. This system would be terrible to use for something that requires a quicker response.

To use -
1. Make sure Python3, Python Requests ([python-requests.org](http://docs.python-requests.org/en/master/)), and miniupnpc ([pypip.org](https://pypi.python.org/pypi/miniupnpc)) are installed.
2. Also make sure miniupnpc is installed through apt in case of an error with the python package `apt install miniupnpc`
3. Copy settings.json.example to settings.json
4. Update settings.json with the Domain, Sub-Domain, [DigitalOcean Authorization Token], and any necessary ports to forward(https://www.digitalocean.com/community/tutorials/how-to-use-the-digitalocean-api-v2)
5. python3 main.py

I have set this to run automatically on every reboot (hence why there's a wait for network section) but this can be run manually as well.